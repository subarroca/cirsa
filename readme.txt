Browsers:
- All modern browsers. Also IE9+

Tech:
- Yeoman (Grunt+Bower)
- AngularJS for js interaction
- CreateJS for reel game

Features:
- Classes for game and player
- Cumulative prizes
- Multiple bets
- Timeouts for real life simulation
- Historic (though not shown)
- String file (possibility to add translations easily)

Future:
- Smooth sprites to slowdown with easing
- Localstorage for history
- Responsiveness
- Testing
- Historic screen of games