'use strict';

angular
  .module('cirsaApp', [
    'ui.router',
    'pascalprecht.translate',
    'restangular'
  ]);

angular
  .module('cirsaApp').config(function($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise('/');
    //
    // Now set up the states
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl as home'
      })
      .state('slot', {
        url: '/play',
        templateUrl: 'views/slot.html',
        controller: 'SlotCtrl as slot'
      });
  });



// Initialize message handlers
angular.module('cirsaApp')
  .config(
    function($translateProvider) {
      // Register a loader for the static files
      // So, the module will search missing translation tables under the specified urls.
      // Those urls are [prefix][langKey][suffix].
      $translateProvider.useStaticFilesLoader({
        prefix: 'json/langs/',
        suffix: '.json'
      });

      // Tell the module what language to use by default
      $translateProvider.preferredLanguage('en');

      // // Tell the module to store the language in the cookies
      // $translateProvider.useCookieStorage();
    }
);


// Initialize API url
angular.module('cirsaApp')
  .config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl('http://cs-testgamingapi.cloudapp.net/api');
  
    RestangularProvider.setResponseExtractor(function(response) {
      var newResponse = response;
      if (angular.isArray(response)) {
        angular.forEach(newResponse, function(value, key) {
          newResponse[key].originalElement = angular.copy(value);
        });
      } else {
        newResponse.originalElement = angular.copy(response);
      }

      return newResponse;
    });
  });
