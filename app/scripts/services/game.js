'use strict';


/**
 * @ngdoc service
 * @name cirsaApp.service:game
 *
 * @description
 * Game object for every play
 */
angular.module('cirsaApp')
  .factory('Game', function Game() {
    var gameDefault = {

      /**
       * @ngdoc property
       * @name cirsaApp.service:game#bet
       * @propertyOf cirsaApp.service:game
       * @returns {int} .
       *
       * @description
       * Bet amount
       */
      bet: '',

      /**
       * @ngdoc property
       * @name cirsaApp.service:game#service
       * @propertyOf cirsaApp.service:game
       * @returns {array} .
       *
       * @description
       * Result of reels
       */
      reels: [],

      /**
       * @ngdoc property
       * @name cirsaApp.service:game#prize
       * @propertyOf cirsaApp.service:game
       * @returns {float} .
       *
       * @description
       * Prize won
       */
      prize: 0,

      /**
       * @ngdoc property
       * @name cirsaApp.service:game#ts
       * @propertyOf cirsaApp.service:game
       * @returns {timestamp} .
       *
       * @description
       * Creation time
       */
      ts: 0
    };





    var GameObj = function(game) {
      angular.extend(this, angular.copy(gameDefault), angular.copy(game));
      this.ts = Date.now();
    };







    return GameObj;
  });
