'use strict';

/*global _*/


/**
 * @ngdoc service
 * @name cirsaApp.service:player
 * @requires cirsaApp.service:game
 *
 * @description
 * player object for sales
 */
angular.module('cirsaApp')
  .factory('Player', function Player() {
    var playerDefault = {

      /**
       * @ngdoc property
       * @name cirsaApp.service:player#games
       * @propertyOf cirsaApp.service:player
       * @returns {array} .
       *
       * @description
       * All games
       */
      games: []
    };





    var PlayerObj = function(player) {
      angular.extend(this, angular.copy(playerDefault), angular.copy(player));
    };





    /**
     * @ngdoc method
     * @name cirsaApp.service:player#addGame
     * @methodOf cirsaApp.service:player
     * @param {cirsaApp.service:game} game Game
     * @return {cirsaApp.service:game} Game after add

     * @description
     * Add a game to the player
     */
    PlayerObj.prototype.addGame = function(game) {
      this.games.push(angular.copy(game));

      return game;
    };





    /**
     * @ngdoc method
     * @name cirsaApp.service:player#getCurrentGame
     * @methodOf cirsaApp.service:player
     * @return {cirsaApp.service:game}

     * @description
     * Get current game
     */
    PlayerObj.prototype.getCurrentGame = function() {
      return _.last(this.games);
    };





    /**
     * @ngdoc method
     * @name cirsaApp.service:player#getTotalPrize
     * @methodOf cirsaApp.service:player
     * @param {cirsaApp.service:game} game Game
     * @return {cirsaApp.service:game} Game after add

     * @description
     * Add a game to the player
     */
    PlayerObj.prototype.getTotalPrize = function() {
      return _.chain(this.games)
        .pluck('prize')
        .reduce(function(sum, num) {
          return sum + num;
        }).value();
    };



    return PlayerObj;
  });
