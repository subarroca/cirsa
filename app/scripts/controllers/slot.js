'use strict';
/*global createjs*/


/**
 * @ngdoc function
 * @name cirsaApp.controller:SlotCtrl
 * @requires $timeout
 * @requires $state
 * @requires Restangular
 
 * @description
 * Slot machine
 */
angular.module('cirsaApp')
  .controller('SlotCtrl', function($scope, $rootScope, $timeout, $state, Restangular) {
    var vm = this;
    var stage,
      w, h,
      reels = [],
      waitSecs = 1.5,
      numReels = 3,
      player,
      game;


    /**
     * @ngdoc property
     * @name cirsaApp.controller:SlotCtrl#totalPrize
     * @propertyOf cirsaApp.controller:SlotCtrl
     * @returns {float}

     * @description
     * Cumulative prize
     */
    vm.totalPrize = 0;

    /**
     * @ngdoc property
     * @name cirsaApp.controller:SlotCtrl#prize
     * @propertyOf cirsaApp.controller:SlotCtrl
     * @returns {float}

     * @description
     * Current prize
     */
    vm.prize = 0;

    /**
     * @ngdoc property
     * @name cirsaApp.controller:SlotCtrl#bet
     * @propertyOf cirsaApp.controller:SlotCtrl
     * @returns {int}

     * @description
     * Number of bets
     */
    vm.bet = 0;

    // Load bet from API
    var getBet = function() {
      Restangular.one('bets', game.bet).get().then(function(resp) {
        game.reels = resp.originalElement.reels;
        game.prize = resp.originalElement.prize;

        stopReel(0);
      });
    };


    // Init stage
    var initCanvas = function() {
      stage = new createjs.Stage('game');

      // grab canvas width and height for later calculations:
      w = stage.canvas.width;
      h = stage.canvas.height;



      // setup spritesheet
      var frames = [];
      for (var i = 0; i < 14; i++) {
        // x, y, width, height, imageIndex, regX, regY
        if (i < 7) {
          frames.push([68 * i, 0, 68, 197]);
        } else {
          frames.push([68 * (i - 7), 197, 68, 197]);
        }
      }

      var data = new createjs.SpriteSheet({
        'images': ['http://cs-testgamingapi.cloudapp.net/images/imagereel.png'],
        'frames': frames,
        // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        'animations': {
          'run': [0, 13, 'run', 1],
          'jump1': [0, 12, false],
          'jump2': [0, 0, false],
          'jump3': [0, 2, false],
          'jump4': [0, 4, false],
          'jump5': [0, 6, false],
          'jump6': [0, 8, false],
          'jump7': [0, 10, false]
        }
      });


      // create central square
      var shape = new createjs.Shape();
      shape.graphics
        .setStrokeStyle(3, 'round')
        .beginStroke('#c00').drawRect(5, 60, 240, 77);


      // init scene
      for (i = 0; i < numReels; i++) {
        reels.push(addReel(data, i));
      }

      stage.addChild(shape);

      createjs.Ticker.setFPS(20);
      createjs.Ticker.addEventListener('tick', stage);
    };


    // Add a reel to the stage
    var addReel = function(data, i) {
      var reel = new createjs.Sprite(data, 'run');
      reel.x = 80 * i + 10;
      reel.y = 0;

      stage.addChild(reel);

      return reel;
    };


    // Stop one reel and go for the next one.
    // On last -> show prize
    var stopReel = function(i) {
      reels[i].gotoAndPlay('jump' + game.reels[i]);
      i++;

      if (i < game.reels.length) {
        // stop next reel
        $timeout(function() {
            stopReel(i);
          },
          waitSecs * 1000);
      } else {
        // show prize
        reels[i - 1].addEventListener('animationend', showPrize);
      }
    };


    // Update model to show prize
    var showPrize = function() {
      vm.prize = game.prize;
      vm.totalPrize = player.getTotalPrize();
      $scope.$apply();
    };


    if (angular.isUndefined($rootScope.player)) {
      // go home and create a bet
      $state.transitionTo('home');

    } else {
      // play game
      player = $rootScope.player;
      game = player.getCurrentGame();

      vm.totalPrize = player.getTotalPrize();
      vm.prize = 0;
      vm.bet = game.bet;

      getBet();
      initCanvas();
    }
  });
