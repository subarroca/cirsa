'use strict';


/**
 * @ngdoc function
 * @name cirsaApp.controller:HomeCtrl
 * @requires $state
 * @requires cirsaApp.service:game
 * @requires cirsaApp.service:player
 
 * @description
 * Home screen
 */
angular.module('cirsaApp')
  .controller('HomeCtrl', function($rootScope, $state, Game, Player) {
    var vm = this;

    if (angular.isUndefined($rootScope.player)) {
      $rootScope.player = new Player();
    }

    /**
     * @ngdoc property
     * @name cirsaApp.controller:HomeCtrl#bet
     * @propertyOf cirsaApp.controller:HomeCtrl
     * @returns {int} .
     * @description
     * Number of bets
     */
    vm.bet = 1;

    /**
     * @ngdoc method
     * @name cirsaApp.controller:HomeCtrl#gotoPlay
     * @methodOf cirsaApp.controller:HomeCtrl
     *
     * @description
     * Add game to player and go to slot page
     */
    vm.gotoPlay = function() {
      $rootScope.player.addGame(new Game({
        bet: vm.bet
      }));

      $state.transitionTo('slot');
    };
  });
